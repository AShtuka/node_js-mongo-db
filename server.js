const express = require('express');
const mongoClient = require('mongodb').MongoClient;
const ObjectId = require('mongodb').ObjectID;
const bodyParser = require('body-parser');
const db = require('./config/db');
const app = express();
const port = 8000;
const client = new mongoClient(db.url, {useNewUrlParser: true,
                                        useUnifiedTopology: true});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


client.connect((err, connection) => {
    if (err){
            return console.log(err);
    }
    require('./app/routes')(app,
                            connection.db('clientdb').collection('notes'),
                            ObjectId);
    app.listen(port, () => {
        console.log('We are live on ' + port);
    });
})
