module.exports = function (app, db, ObjectID) {

    app.get('/notes/:id', (req, res) => {
        const id = req.params.id;
        const details = {'_id': new ObjectID(id)};
        db.findOne(details, (err, item) => {
            if (err) {
                res.send({'error': 'An error has occurred'})
            } else {
                res.send(item);
            }
        })
    });

    app.delete('/notes/:id', (req, res) => {
        const id = req.params.id;
        const details = {'_id': new ObjectID(id)};
        db.remove(details, (err, item) => {
            if (err) {
                res.send({'error': 'An error has occurred'})
            } else {
                res.send('Note ' + id + ' deleted!');
            }
        })
    });

    app.put('/notes/:id', (req, res) => {
        const id = req.params.id;
        const details = {'_id': new ObjectID(id)};
        const note = {'name': 'Barni'};
        db.update(details, note, (err, result) => {
            if (err) {
                res.send({'error': 'An error has occurred'})
            } else {
                res.send(note);
            }
        })
    });

    app.post('/notes', (req, res) => {
        const note = { name: req.body.name, surName: req.body.surName };
        db.insert(note, (err, result) => {
            if (err) {
                res.send({'error': 'An error has occurred'})
            } else {
                res.send(result.ops[0]);
            }
        })
    });
};